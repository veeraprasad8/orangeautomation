package driverbase;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import io.github.bonigarcia.wdm.WebDriverManager;
import utilities.CommonUtility;

public class TestBase {
	
	
	public Properties prop;
	public static WebDriver driver;
	
	
	@BeforeSuite
	public void readProperteis() throws Exception {
		
		prop = new Properties();
		
		FileInputStream fs = new FileInputStream("C:\\Users\\user\\eclipse-workspace\\automation\\src\\main\\java\\properties\\config.properties");
		
		prop.load(fs);
		
	}
	
	@BeforeTest
	public void browserLaunch() {
		
		if(prop.get("browser").equals("chrome")) {
			WebDriverManager.chromedriver().setup();
			driver = new ChromeDriver();
			
		}
		
		else if(prop.get("browser").equals("firefox")) {
			
			WebDriverManager.firefoxdriver().setup();
			driver = new FirefoxDriver();
		}
		
		
		else {
			
			System.out.println("please specity the correct browser");
		}
		driver.get(prop.getProperty("appUrl"));
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}
	
	
	
	@AfterMethod
	public void takesScreenShotForFailureTest(ITestResult result) throws IOException {
		
		if(ITestResult.FAILURE == result.getStatus()) {
			
			CommonUtility.takesScreenShot(result.getName());
			
		}}
	
	
	@AfterTest
	public void closeBrowser() {
		
		driver.quit();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	

}
