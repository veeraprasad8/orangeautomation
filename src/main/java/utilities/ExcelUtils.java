package utilities;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelUtils {
	
	XSSFWorkbook workbook;
	XSSFSheet sheet;
	XSSFRow row;
	XSSFCell cell;
	String fileName;
	FileInputStream fs;
	
	
	public ExcelUtils(String fileName) throws IOException {
		this.fileName = fileName;
		 fs = new FileInputStream(fileName);
		 workbook  = new XSSFWorkbook(fs);
			
	}
	
	public int getRowCount(String sheetName) {
		
		sheet = workbook.getSheet(sheetName);
		return sheet.getLastRowNum();
		
	}
  public int getColumnCount(String sheetName) {
		
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(0);
		return row.getLastCellNum();
		
	}
  
  
  public String getCellData(String sheetName, int rowNum, int columnNum) {
	  
	  
	  sheet = workbook.getSheet(sheetName);
	  row = sheet.getRow(rowNum);
	  cell = row.getCell(columnNum);
	  return cell.getStringCellValue();
	  
  }
	

}
