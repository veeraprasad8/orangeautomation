package utilities;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import driverbase.TestBase;

public class CommonUtility extends TestBase{
	
	
	
	public static void takesScreenShot(String testName) throws IOException {
		
		TakesScreenshot ts = (TakesScreenshot)driver;
		
		File src = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(src, new File("C:\\Users\\user\\eclipse-workspace\\automation\\screenshots\\"+testName+".png"));		
}
	
	public static void selectValueDropDown(WebElement ele, String value) {
		
		Select sel = new Select(ele);
		sel.selectByVisibleText(value);
		
	}
	
	
    public static String getData(int rowNumber, int columnNumber) throws Exception, IOException {
		
		XSSFWorkbook xf = new XSSFWorkbook(new File("C:\\Users\\user\\eclipse-workspace\\automation\\src\\main\\java\\testdata\\data.xlsx"));
		
		org.apache.poi.ss.usermodel.Sheet sh = xf.getSheet("Sheet1");
		Row rw = sh.getRow(1);
		Cell ce = rw.getCell(0);
		String value = ce.getStringCellValue();
		return value;
	}
	
	
}