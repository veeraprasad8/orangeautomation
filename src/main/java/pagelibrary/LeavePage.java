package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import utilities.CommonUtility;

public class LeavePage {
	
	WebDriver driver;
	
	@FindBy(name = "assignleave[txtLeaveType]")
	WebElement leaveDropDown;
	
	public LeavePage(WebDriver driver) {
		
		this.driver = driver;
	}
	
	
	public void selectDropDown(String value) {
		
		CommonUtility.selectValueDropDown(leaveDropDown, value);
		
	}
	
	
	

}
