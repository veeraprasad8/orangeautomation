package pagelibrary;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class DashBoard {
	
	WebDriver driver;
	
	@FindBy(xpath = "//h1[text()='Dashboard']")
	WebElement dashBoardLable;
	
	@FindBy(xpath = "//span[text()='Assign Leave']")
	WebElement assignLeave;
	
	 public DashBoard(WebDriver driver) {
		 this.driver = driver;
	}
	
	public String getTextDashBoard() {
		
		return dashBoardLable.getText();
	}
	
	
	public void verifyAssignLeave() {
		assignLeave.click();
		
		
		
	}
	
	
	
	

}
