package testcases;

import java.io.File;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.sl.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import driverbase.TestBase;
import pagelibrary.DashBoard;
import pagelibrary.LeavePage;
import pagelibrary.LoginPage;
import utilities.CommonUtility;
import utilities.ExcelUtils;

public class LoginClassTest extends TestBase{
	
	LoginPage loginPage;
	DashBoard dashBoardPage;
	LeavePage leavePage;
	
	
	@DataProvider(name = "data")
	public Object[][] getCellData() throws IOException{
		
		Object[][] data = new DataTest().getData("Sheet1");
		return data;
	}

	
	@Test(priority = 0, dataProvider = "data")
	public void verifyLoginFunctionality(String uname, String pwd) throws IOException, Exception {
		
		loginPage = PageFactory.initElements(driver, LoginPage.class);
		
		boolean result = loginPage.isImageExist();
		
		// veirfy the image exist or not
		
		Assert.assertTrue(result, "verify the image is exist or not");
		
		
		loginPage.loginIntotheApplication(uname, pwd);
		if(uname.equals("Admin") & pwd.equals("admin123")){
			System.out.println("user successfully logged in ..|");
		}
		else {
		Assert.assertTrue(loginPage.isErrorMsgExists());
		}
	
		
	}
	
	//@Test(priority = 1)
	public void verifyDashBoardPage() {
		
		dashBoardPage = PageFactory.initElements(driver, DashBoard.class);
		String txt = dashBoardPage.getTextDashBoard();
		
		Assert.assertEquals("Dashboard", txt);
		
		dashBoardPage.verifyAssignLeave();
		String expURL = "https://opensource-demo.orangehrmlive.com/index.php/leave/assignLeave";
		
		Assert.assertEquals(expURL, driver.getCurrentUrl());
		
		
		
		
	}
	//@Test(priority = 2)
	public void verifyLeavePage() {
		
		leavePage = PageFactory.initElements(driver, LeavePage.class);
		leavePage.selectDropDown("CAN - FMLA");
		
		
		
		
	}
	
	
	
	
	
	
	
	

}
