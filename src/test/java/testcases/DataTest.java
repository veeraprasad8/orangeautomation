package testcases;

import java.io.IOException;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import utilities.ExcelUtils;

public class DataTest {
	
	
	
	public Object[][] getData(String sheetName) throws IOException{
		String filePath = "C:\\\\Users\\\\user\\\\eclipse-workspace\\\\automation\\\\src\\\\main\\\\java\\\\testdata\\\\data.xlsx";
		ExcelUtils ex = new ExcelUtils(filePath);
		int rowNum = ex.getRowCount(sheetName);
		int colCount = ex.getColumnCount(sheetName);
		System.out.println(rowNum);
		System.out.println(colCount);
		Object data[][] = new Object[rowNum][colCount];
		for(int i = 1; i<=rowNum;i++) {
			
			for(int j = 0;j<colCount;j++) {
				
				data[i-1][j] = ex.getCellData(sheetName, i, j);	
				
			}
		}
		
		return data;
		
	}
	
	
	
	@Test(dataProvider = "data")
	public void test(String username, String pwd) throws IOException {
		
		System.out.println(username);
		System.out.println(pwd);
		
		
	}
	
	

}
